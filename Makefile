upload: ciLogin buildImages
ciLogin:
	if [ "$$CI_REGISTRY_USER" != ""  ] && [ "$$CI_REGISTRY_PASSWORD" != "" ]; then\
        docker login -u $$CI_REGISTRY_USER -p $$CI_REGISTRY_PASSWORD registry.gitlab.com;\
	fi
buildImages: bookworm
bookworm:
	docker build -f Dockerfile.bookworm -t registry.gitlab.com/fagforbundet/pleiar.no-ci-docker:20-bookworm .
	docker push registry.gitlab.com/fagforbundet/pleiar.no-ci-docker:20-bookworm
