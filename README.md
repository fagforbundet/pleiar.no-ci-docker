# pleiar.no CI Dockerfile

This repository contains the Dockerfile for the docker image used to run CI
tests on pleiar.no. It is basically just a modified node image with some
additional libraries and tools added.

See [gitlab.com/zerodogg/pleiar.no](https://gitlab.com/zerodogg/pleiar.no/) for
the actual source for [pleiar.no](https://www.pleiar.no) (this repository is
just for the CI Dockerfile).

## License

Pleiar.no is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.
